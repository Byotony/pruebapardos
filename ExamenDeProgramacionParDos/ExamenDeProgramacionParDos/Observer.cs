﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenDeProgramacionParDos
{
    // Se crea una clase abstracta con el nombre (Observer)
    abstract class Observer

    {
        // Definimos de forma abstracta Update.
        public abstract void Update();
    }
}
