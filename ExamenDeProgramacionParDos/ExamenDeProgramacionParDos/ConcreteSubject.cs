﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenDeProgramacionParDos
{
    //Creamos una clase Sujeto concreto (ConcreteSubject), la cual tendrá la respectiva herencia de la clase padre (Subject).
    class ConcreteSubject : Subject
    {
        private string _subjectState;

        // Realizamos alguna acciones (get,set)
        //Estas son emitidas por el SubjectState.
        public string SubjectState
        {
            get { return _subjectState; }
            set { _subjectState = value; }
        }
    }
}
