﻿using System;

namespace ExamenDeProgramacionParDos
{
    class Program
    {
        static void Main(string[] args)
        {
            // Creamos los objetivos para poder ejecutar el programa
            ConcreteSubject s = new ConcreteSubject();
            s.Attach(new ConcreteObserver(s, "X"));
            s.Attach(new ConcreteObserver(s, "Y"));
            s.Attach(new ConcreteObserver(s, "Z"));

            //Cambia el estado y notifica

            s.SubjectState = "ABC";
            s.Notify();
            Console.ReadKey();
        }
    }
}