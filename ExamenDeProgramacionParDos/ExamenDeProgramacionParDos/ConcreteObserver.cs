﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenDeProgramacionParDos
{
    //Creamos una clase Sujeto concreto (ConcreteObserver), la cual tendrá la respectiva herencia de la clase padre (Subject).
    class ConcreteObserver : Observer
    {
        // Añadimos propiedad a nuestra clase
        private string _name;
        private string _observerState;
        private ConcreteSubject _subject;
        // Creamos el constructor
        public ConcreteObserver(ConcreteSubject subject, string name)
        {
            this._subject = subject;
            this._name = name;
        }
        // Creamos el metodo de Override, el cual constará de la actualización del estado del observador.
        public override void Update()
        {
            _observerState = _subject.SubjectState;
            Console.WriteLine("El nuevo estado del observer {0} es {1}", _name, _observerState);
        }
        // Creamos el método ConcreteSubject.
        public ConcreteSubject Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }
    }
}

