﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenDeProgramacionParDos
{
        // Creamos una clase abstracta con el nombre del Sujeto (Subject)
        abstract class Subject
        {
            // Se creamos una lista para los sujetos
            private List<Observer> _observers = new List<Observer>();
            
            // Aquí aplicamos los métodos que nos permitirán gestionar los sujetos
            public void Attach(Observer observer)
            {
                _observers.Add(observer);
            }
            public void Detach(Observer observer)
            {
                _observers.Remove(observer);
            }
            // Nos permite crear una actualización para el sujeto, dependiente del estado del observador.
            public void Notify()
            {
                foreach (Observer o in _observers)
                {
                    o.Update();
                }
            }
    }
}
